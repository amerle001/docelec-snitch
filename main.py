from snitchLib import makeProxyUrlsList, writeReport

urlsPortail = ["https://bibliotheques.univ-pau.fr/fr/documentation/bases-de-donnees.html", "https://bibliotheques.univ-pau.fr/fr/documentation/livres-electroniques.html",
               "https://bibliotheques.univ-pau.fr/fr/documentation/encyclopedies-et-dictionnaires.html"]


proxyLinks = makeProxyUrlsList(urlsPortail)

print(f"Nombre de BDD sur la page : {len(proxyLinks)}")

f = open("liste-liens.txt", "w")
for url in proxyLinks:
    f.write(f"{url}\n")
f.close


print(
    f"Nombre de BDD sur la page après élimination des doublons : {len(proxyLinks)}")

writeReport(proxyLinks)
