from requests.sessions import Request
import browser_cookie3
import urllib.request
import requests
import ssl
from bs4 import BeautifulSoup


def findTitle(url):
    webpage = urllib.request.urlopen(url).read()
    title = str(webpage).split('<title>')[1].split('</title>')[0]
    return title

# getTitles à transformer en getProofOfConnect
# prend url et bonne balise à vérifier (un dictionnaire ou un tuple k,v)


def getTitles(url):
    cj = browser_cookie3.firefox()
    opener = urllib.request.build_opener(
        urllib.request.HTTPCookieProcessor(cj))
    opener.addheaders = [('User-agent', 'Mozilla/5.0')]
    httpCode = opener.open(url).getcode()
    print(httpCode)
    if httpCode >= 400:
        return "down"
    webpage = opener.open(url).read()
    title = str(webpage).split('<title>')[1].split('</title>')[0]
    return title

# pour chaque url, on regarde dans le html si il existe une marque distinctive
# il faut faire un array de marque distinctive
# il faut aussi faire un test spécifique pour le texte alternatif
# ou les images


def getUPPAMark(url, marques):
    cj = browser_cookie3.firefox()
    opener = urllib.request.build_opener(
        urllib.request.HTTPCookieProcessor(cj))
    opener.addheaders = [('User-agent', 'Mozilla/5.0')]
    webpage = opener.open(url).read()

    # soup = BeautifulSoup(webpage, 'html.parser')
    for m in marques:
        # print(soup(text=m))
        if m.lower() in str(webpage).lower():
            # if soup(text=lambda t: m in t):
            print("MARQUE TROUVEE")
            return True
        else:
            print("--")
    return False
