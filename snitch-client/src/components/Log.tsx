import { useState, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";

const Log = () => {
  const [log, setLog] = useState<Array<string>>([""]);
  useEffect(() => {
    const fetchLogs = async () => {
      fetch("../../../rapport.txt")
        .then((r) => r.text())
        .then((t) => t.split("\n"))
        .then((t) => setLog(t));
    };
    // const intervalTimer = setInterval(fetchLogs, 1000);
    // return () => clearInterval(intervalTimer);
    fetchLogs();
  }, []);
  return (
    <div className="terminal">
      {log[0] === "" ? (
        <>
          <h3>Docelec Snitch</h3>
          <p>Scan en cours, veuillez patienter...</p>
        </>
      ) : (
        log.map((ligne) => <p key={uuidv4()}>{ligne}</p>)
      )}
    </div>
  );
};

export default Log;
