from os import error
import time
import requests
from bs4 import BeautifulSoup
from testUrl import getTitles, getUPPAMark


marques = list(dict.fromkeys(["UNIV DE PAU", "UPPA - Univ Pau", "Univ de Pay et des Pays de l'Adour", "Univ de Pau", "AJ3F8E98", "UNIV DE PAU", "UNIVERSITE DE PAU", "Univ de Pay et des Pays de l'Adour", "UNIVERSITE PAU & PAYS DE L ADOUR", "Universit de Pau et des Pays de l'Adour", "Université de Pau et des Pays de l'Adour", "Université de Pau et des Pays de l'Adour", "SCD de l'UPPA", "Université de Pau et des Pays de l'Adour", "UNIVERSITE DE PAU & DES PAYS DE", "Bienvenue UPPA", "Universite de Pau",
                              "EFL64UNIVERSI1IK", "Bienvenue UNIVERSITE DE PAU", "Universite De Pau Et Des Pays De l'Adour", "Bib. Pau", "Universite De Pau Et Des Pays De L'Adour", "UNIV DE PAU ET DU PAYS DE L'ADOUR IP", "Bib. Pau", "Network access provided by: Université de Pau et des Pays de L'Adour", "Brought to you by:UPPA", "Access provided by Université de Pau & des Pays de l'Adour", "Univ de Pay et des Pays de l'Adour", "Universite De Pau Pays De L'adour", "Universite de Pau et des Pays de l'Adour"]))
# prend array de pages web de documentation électronique et renvoit un array consolidé et dédoublonné d'urls proxifiées


def makeProxyUrlsList(urlArray):
    allLinks = []
    proxyLinks = []

    for url in urlArray:
        r = requests.get(url)
        html_content = r.text
        soup = BeautifulSoup(html_content, 'html.parser')
        soup.find_all('a')

        for link in soup.find_all('a'):
            allLinks.append(link.get('href'))
            if "rproxy" in str(link.get('href')):
                proxyLinks.append(link.get('href'))

    # élimination des doublons
    return list(dict.fromkeys(proxyLinks))


def writeReport(listeDeLiens):

    f = open("./public/rapport.txt", "w")
    named_tuple = time.localtime()
    time_string = time.strftime("%d/%m/%Y, %H:%M:%S", named_tuple)

    f.write("************************************************************************************\n")
    f.write("*******************************   DOCELEC SNITCH   *************************************\n")
    f.write(
        f"****************************   {time_string}   **********************************\n")
    f.write("************************************************************************************\n")
    okCount = 0
    marqueCount = 0
    downCount = 0
    CASCount = 0
    ScriptExceptionsCount = 0

    recalcitrants = []

    for url in listeDeLiens:
        try:
            token = getTitles(url)
            if token == "down":
                f.write("LIEN_CASSE\t\t\t\t\t")
                f.write(f"{url}\n")
                downCount += 1

            if token == "Login - CAS &#8211; Central Authentication Service":
                f.write("CAS_SCREEN\t\t\t")
                f.write(f"{url}\n")
                CASCount += 1
            else:
                if getUPPAMark(url, marques):
                    f.write("OK++ (marque)\t\t")
                    f.write(f"{url}\n")
                    marqueCount += 1
                    okCount += 1
                else:
                    f.write("OK\t\t\t\t\t")
                    f.write(f"{url}\n")
                    okCount += 1
        except Exception as e:
            print(e)
            f.write("ERR_SCRIPT\t\t")
            f.write(f"{url}\n")
            recalcitrants.append(url)
            ScriptExceptionsCount += 1
            continue

    named_tuple = time.localtime()
    time_string = time.strftime("%d/%m/%Y, %H:%M:%S", named_tuple)
    time_string = time.strftime("%d/%m/%Y, %H:%M:%S", named_tuple)

    f.write("\n\n************************************************************************************\n")
    f.write("*******************************   RAPPORT DE TEST   ************************************\n")
    f.write(
        f"****************************   {time_string}   **********************************\n")
    f.write("************************************************************************************\n")

    f.write(
        f"Sur {len(listeDeLiens)} ressources testées, {okCount} sont OK. {marqueCount} ont renvoyé une marque d'abonnement.\n")
    f.write(f"Liens cassés : {downCount}\n")
    f.write(f"Echec de connexion (CAS) : {CASCount}\n")
    f.write(
        f"Ressources récalcitrantes (refusent les connexions du script) : {ScriptExceptionsCount}\n")
    f.write("Listes des bases récalcitrantes à vérifier manuellement   :\n")
    for base in recalcitrants:
        f.write(f"{base}\n")

    f.write("\n\n************************************************************************************\n")
    f.write("*******************************   DEBUG   **********************************************\n")
    f.write("************************************************************************************\n")
    f.write(f"Nombre de ressources testées : {len(listeDeLiens)}\n")
    f.write(f"Nombre de marques distinctives : {len(marques)}\n")

    f.close
